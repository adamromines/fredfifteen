<?php get_header(); ?>
<div class="container">
  <div class="row">
    <div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-body">
		      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		        <h1><?php the_title(); ?></h1>
		        <?php the_content(); ?>

		      <?php endwhile; else: ?>
		        <p><?php _e('Sorry, that page does not exist.'); ?></p>
		      <?php endif; ?>
				</div><!--/ panel body -->
			</div><!--/ panel -->
    </div> <!--/ main column (col-md-8) -->
    <div class="col-md-4">

			<div class="panel panel-default">
				<div class="panel-body">
					<?php
					     if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('do_right') ) :
					    endif; ?>
				</div><!--/ panel body -->
			</div><!--/ panel -->
    </div><!--/ sidebar (col-md-4) -->
  </div><!--/ row -->


<?php get_footer(); ?>
