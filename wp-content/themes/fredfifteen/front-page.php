<?php get_header();?>

<?php

$args = array(
	'post_type' => 'gallery',
	'p' => '253'
);

// The Query
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {
	// Open main carousel div ?>
	<div id="theCaro" class="carousel slide" data-ride="carousel">
	  <ol class="carousel-indicators">
	    <li data-target="#theCaro" data-slide-to="0" class="active"></li>
	    <li data-target="#theCaro" data-slide-to="1"></li>
	    <li data-target="#theCaro" data-slide-to="2"></li>
	    <li data-target="#theCaro" data-slide-to="3"></li>
	  </ol>

		<?php

		function ensure_ssl($url)
		{
			return str_replace('http://','https://', $url);
		}


		while ( $the_query->have_posts() ) {
			$the_query->the_post();

			$images = get_field('images');

			if ($images) { ?>
				<div class="carousel-inner" role="listbox">
				 <?php

				foreach( $images as $index=>$image ) {
					// if on the first go round add 'active' class to item div
					if ($index==0) { ?>
						<div class="item active">
						<?php } else { ?>
						<div class="item">
						<?php } ?>
							<img class="bg" src="<?php echo ensure_ssl($image['sizes']['medium']); ?>" srcset="<?php echo ensure_ssl($image['sizes']['large']);?> 480w, <?php echo ensure_ssl($image['sizes']['large']);?> 760w" alt="<?php echo $image['alt']; ?>" />
				 		</div>
				<?php } ?>
				</div> <!-- /div.carousel-inner -->
				 <?php
		} else {
			echo 'looks like no images were found in the custom field';
		} ?>
		  <!-- Left and right controls -->
		  <a class="left carousel-control" href="#theCaro" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#theCaro" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div> <!-- close main carousel div -->

		 <?php
	}

} else {
	// fallback if query returns no posts
	echo 'There are supposed to be pictures here. Please <a href="/contact">contact us</a> and let us know something went wrong';
}
/* Restore original Post Data */
wp_reset_postdata();

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <?php the_content(); ?>

<!--
Play pause buttons for developer sanity -->
<div id="carouselButtons">
    <button id="playButton" type="button" class="btn btn-default btn-xs">
        <span class="glyphicon glyphicon-play"></span>
     </button>
    <button id="pauseButton" type="button" class="btn btn-default btn-xs">
        <span class="glyphicon glyphicon-pause"></span>
    </button>
</div>

<?php endwhile; else: ?>
  <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
