
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="../../favicon.ico"> -->

    <title>Frederick Landscaping LLC | Jackson, WY</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<?php wp_head(); ?>
  </head>

  <body <?php body_class( ); ?> >	
		
	  <nav class="navbar navbar-default">
	  	<div class="container-fluid">
		    <div class="navbar-header">
					<span id="phone">307.730.0037</span><br>
		      <a class="logo" href="/">
						<img id="brandImg" src="<?php echo get_template_directory_uri(); ?>/public/images/Frederick-Landscaping-LLC.png">
					</a>
					<span id="pRT">Performance. Reliability. Trust.</span>
					
		      <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target="#theNavbar">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>                        
		      </button>

		    </div><!--/.navbar-header -->
				
				<div class="collapse navbar-collapse" id="theNavbar">
	        <ul class="nav navbar-nav" id="theNav">
	          <?php wp_list_pages(array('title_li' => '', 'exclude' => '15')); ?>
	        </ul>
				</div><!--/#myNavbar -->
	      <div class="clearfix"></div>
	  	</div><!--/.container-fluid -->
	  </nav>
		
		
		

