<?php
/*
	Template Name: Gallery Overview
*/

?>
<?php get_header(); ?>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Our Work
                <small>A sampling of our projects</small>
            </h1>
        </div>
    </div>
    <!-- /.row -->

		<?php

		$paged = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;

		$args = array(
			'post_type' => 'gallery',
			'posts_per_page' => '9',
			'post__not_in' => array(253),
			'paged' => $paged

		);

		$query = new WP_Query( $args );

		?>

  <!-- Projects Row -->
	  <div class="row">

		<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

      <div class="col-md-4 portfolio-item">
		<div class="panel panel-default">
          <a href="<?php the_permalink(); ?>" class="gallery-thumb-wrapper">
              <?php $image_arr = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' ); ?>
              <img class="img-responsive gallery-thumb" src="<?php echo $image_arr[0]; ?>" alt="">
          </a>
          <h3>
              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h3>
          <p><?php

          $description = get_field('description');

          echo wp_trim_words($description, $num_words = 32, $more = '...');

          ?>
          </p>
		</div>
      </div>


		<?php endwhile; ?>

		<div class="col-md-12">
			<div class="pagi nav-next lefty"><?php previous_posts_link( 'Previous' ); ?></div>
			<div class="pagi nav-previous poncho"><?php next_posts_link( 'See more', $query->max_num_pages ); ?></div>
		</div>

		<?php else: ?>
		  <p><?php _e('Sorry, we couldn\'t find any galleries'); ?></p>
		<?php endif; wp_reset_postdata(); ?>



	  </div>
	  <!-- /.row -->

</div>
<!-- /.container -->

<?php get_footer(); ?>
