


    </div> <!-- /container -->
    <footer>
      <hr>
      <div class="container">
        <div class="bbb">
          <a target="_blank" title="Frederick Landscaping LLC BBB Business Review" href="http://www.bbb.org/snakeriver/business-reviews/landscape-contractors/frederick-landscaping-llc-in-jackson-wy-1000018276/#bbbonlineclick"><img alt="Frederick Landscaping LLC BBB Business Review" style="border: 0;" src="http://seal-boise.bbb.org/seals/blue-seal-96-50-frederick-landscaping-llc-1000018276.png" /></a>
        </div>
          <?php
          /*
          	Lazily loaded facebook like button
          */
            $wp_obj_id = get_queried_object_id();
            $page_permalink = get_permalink($wp_obj_id);
            $page_title = get_the_title($wp_obj_id);
            $fb_share_url = "http://www.facebook.com/sharer.php?u=" . urlencode($page_permalink);
          ?>
        <div class="fb_foot">
          <a href="<?php echo $fb_share_url; ?>" class="socialite facebook-like" data-href="<?php echo $page_permalink; ?>" data-send="false" data-layout="box_count" data-width="60" data-show-faces="false" rel="nofollow" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/public/images/like_load.png"></a>
        </div>


        <p>&copy; Frederick Landscaping, <?php echo date("Y") ?></p>

      </div>
    </footer>
	<?php wp_footer(); ?>

  </body>
</html>
