const gulp = require('gulp');
const plumber = require('gulp-plumber');
const less = require('gulp-less');
const mainBowerFiles = require('main-bower-files');
const gulpFilter = require('gulp-filter');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const minifyCSS = require('gulp-minify-css');
const browserSync = require('browser-sync');
const merge = require('merge2');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del')
// const using = require('gulp-using');

const server = browserSync.create();

const filterByExtension = (extension) => {
    return gulpFilter((file) => {
        return file.path.match(new RegExp('.' + extension + '$'));
    });
};

bowerFiles = mainBowerFiles();
dest = 'public/';

const cleanScripts = () => del(['public/js']);
const cleanStyles = () => del(['public/css']);

function vendorScripts () {

    if (!bowerFiles.length) {
        console.log('No bower files found');
        return Promise.resolve();
    }

    return gulp.src(bowerFiles)
        .pipe(filterByExtension('js'))
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dest + 'js'))
}


function mainScripts () {
    return gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest(dest + 'js'))

}

// CSS
function css () {

    var cssFilter = gulpFilter('**/*.css');

    var styles = gulp.src('src/css/*.less')
        .pipe(sourcemaps.init())
        .pipe(less());

    var vendorStyles = gulp.src(bowerFiles)
        .pipe(cssFilter);

    return merge(vendorStyles, styles)
        .pipe(concat('fredfifteen.css'))
        .pipe(sourcemaps.write())
        .pipe(minifyCSS())
        .pipe(gulp.dest(dest + 'css'))
        .pipe(server.stream());

};

gulp.task('fonts', function () {
    return Promise.all([
        gulp.src('src/vendor/**/*.{ttf,woff,eof,svg}')
            .pipe(gulp.dest(dest + 'fonts')),
        gulp.src('src/fonts/*.{ttf,woff,eof,svg,eot}')
            .pipe(gulp.dest(dest + 'fonts'))
    ])
});

// Watch
function watch (done) {
    gulp.watch('src/js/*.js', gulp.series(cleanScripts, gulp.parallel(mainScripts, vendorScripts), reload));
    gulp.watch('src/css/*.less', gulp.series(cleanStyles, css));
    done();
}


// BrowserSync
function serve(done) {
    server.init({
        proxy: "http://frederick.local"
    });
    done();
}

function reload(done) {
    server.reload();
    done();
}

gulp.task('build', gulp.series(gulp.parallel(cleanStyles, cleanScripts), gulp.parallel(mainScripts, vendorScripts, css)));

gulp.task('default', gulp.series(gulp.parallel(cleanStyles, cleanScripts), serve, gulp.parallel(mainScripts, vendorScripts, css), watch));
