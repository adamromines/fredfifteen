<?php get_header(); ?>

<div class="bodyContent">

<div class="title-container">
    <span class="back">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <a href="/services-jackson-hole-landscape">Back to Services</a>
    </span>
    <h1><?php single_tag_title(); ?></h1>
</div>

<?php

    $images = get_field('images');
    $description = get_field('description');
    $slug = get_queried_object()->slug;
    $args = array(
        'post_type' => 'attachment',
        'post_mime_type' =>'image',
        'post_status' => 'any',
        'tag' => $slug,
    );
    // $query = new WP_Query( $args );
    $images = get_posts( $args );

    if ( $images ) : ?>
        <div id="slider" class="flexslider">
            <ul class="slides">
                <?php foreach( $images as $image ): ?>
                    <li>
                        <img src="<?php echo wp_get_attachment_image_src($image->ID, 'full')[0]; ?>" alt="" />

                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div id="carousel" class="flexslider">
            <ul class="slides">
                <?php foreach( $images as $image ): ?>
                    <li>
                        <img src="<?php echo wp_get_attachment_image_src($image->ID, 'thumbnail')[0]; ?>" alt="" />
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php
    else : ?>
        <h2>No images match this tag. Sorry!</h2>
    <?php
    endif; ?>



</div>
<?php get_footer(); ?>
