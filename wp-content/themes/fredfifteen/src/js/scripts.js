//
// Homepage
//


//
// Gallery
//

$(document).on('ready', function () {
	$('.navLinks > li > a').eq(4).text('Services');

});


$(window).on('load', function () {

	// Gallery Overview
	// Turn wordpress pagination anchor into bootstrap button

	if ($('body').hasClass('page-id-7')) {
		$('.pagi a').addClass('btn btn-lg');
	}

	// Single Gallery or Tag

	if ($('body').hasClass('single-gallery') || $('body').hasClass('tag')) {

		var vpHeight = $(window).height() - $('#theHeader').height();
		fixExtraWide(vpHeight);

		// Setup Flexslider(s)
		var $slider = $('#slider');
		var $carousel = $('#carousel');

		var carouselOptions = {
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 180,
			itemMargin: 5,
			asNavFor: '#slider',
			multipleKeyboard: true
		};

		// The slider being synced must be initialized first
		$carousel.flexslider(carouselOptions);

		$slider.flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			sync: "#carousel",
			multipleKeyboard: true
		});

		fixPortraits(vpHeight);
	}



	// setTimeout(fixTenPxGap, 500);

	function fixExtraWide(vpHeight) {
		// Confronted with an extra wide viewport?
		// If bottom of slider is cut off ( viewport ratio > 3/2 ),
		// determine available heigh (limiting dimmension) and base width on  that

		var vpWidth = $('.bodyContent').width();
		var vpRatio = vpWidth / vpHeight;


		// if viewport ratio is wider than 3x2, determine desired
		// width (1.5 times available height)
		if (vpRatio > 1.5) {

			var pixelWidth = vpHeight * 1.26;

			// what percentage of the available (viewport) width is that?
			var percentWidth = ((pixelWidth / vpWidth) * 100).toFixed(3);

			var widthString = percentWidth.toString() + '%';

			$('#slider').width(widthString);

		}

		// available width / desired slider width (pixels)
		// = percentage width to assign to slider

	}


	function fixPortraits(vpHeight) {
		// Identify images with portrait orientation, set height to 2/3 slider
		// width and center them

		var sliderWidth = $('#slider').width();


		var $portraitImages = $('.slides img')
			.filter(function () {
				var height = $(this).get(0).naturalHeight;
				var width = $(this).get(0).naturalWidth;

				return height > width;
			});

		$portraitImages.height(.666667 * sliderWidth).width('auto').css('margin', '0 auto');
	}

	// // fix image heights after window resize event is finished
	// (function(){
	//     function doneResizing(){
	// 			fixExtraWide();
	//     }
	//     var takeTime;
	//     $(window).resize(function() {
	//         clearTimeout(takeTime);
	//         takeTime = setTimeout(doneResizing, 500);
	//     });
	// })();

	Socialite.load();

	$('#playButton').click(function () {
		$('.carousel').carousel('cycle');
		console.log('play button clicked');
	});
	$('#pauseButton').click(function () {
		$('.carousel').carousel('pause');
		console.log('pause button clicked');
	});

});
