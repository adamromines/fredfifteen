<?php

function my_scripts_method() {
	// deregister built in jquery
	wp_deregister_script('jquery');
	// this emoji script was http, so just throw it out
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	// register and enqueue vendor and theme styles
	wp_register_script('vendor_js', get_template_directory_uri() . '/public/js/vendor.js', array(), null, false);
	wp_register_script('main_js', get_template_directory_uri() . '/public/js/main.js', array(), null, false);
	wp_enqueue_script('vendor_js', get_template_directory_uri() . '/public/js/vendor.js', array(), null, false);
	wp_enqueue_script('main_js', get_template_directory_uri() . '/public/js/main.js', array(), null, false);

	// enqueue styles
	wp_enqueue_style( 'gFonts', 'http://fonts.googleapis.com/css?family=Arvo|EB+Garamond|Armata' );
	wp_enqueue_style( 'fredfifteen', get_template_directory_uri() . '/public/css/fredfifteen.css' );

}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

/**
 * Ensure WordPress responsive images use https
 *
 */

function make_srcset_ssl($sources) {
	$filtered_sources = array();
	foreach($sources as $source_key=>$source) {
	  $source['url'] = str_replace('http://','https://', $source['url']);
	  $filtered_sources[$source_key] = $source;
	}
	return $filtered_sources;
  }

add_filter( 'wp_get_attachment_url', 'set_url_scheme' );
add_filter( 'wp_calculate_image_srcset', 'make_srcset_ssl');

// Register our sidebar and widgetized areas.

function fredfifteen_widgets_init() {

	if ( function_exists('register_sidebar') ) {

		register_sidebar( array(
			'name'          => 'Bud and Sam',
			'id'            => 'bud_sam',
			'before_widget' => '<div class="sidebarWrap">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => 'Do it Right',
			'id'            => 'do_right',
			'before_widget' => '<div class="sidebarWrap">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => 'Contact Info',
			'id'            => 'contact',
			'before_widget' => '<div class="sidebarWrap">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		) );

	}



}
add_action( 'widgets_init', 'fredfifteen_widgets_init' );



// apply tags to attachments
function wptp_add_tags_to_attachments() {
    register_taxonomy_for_object_type( 'post_tag', 'attachment' );
}
add_action( 'init' , 'wptp_add_tags_to_attachments' );

//thumbnails
add_theme_support( 'post-thumbnails' );

/**
 *
 *
 */

add_filter("retrieve_password_message", "map_custom_password_reset", 99, 4);

	function map_custom_password_reset($message, $key, $user_login, $user_data )    {

	$message = "Someone has requested a password reset for the following account:

	" . sprintf(__('%s'), $user_data->user_email) . "

	If this was a mistake, just ignore this email and nothing will happen.

	To reset your password, visit the following address:

	" . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . "\r\n";


		return $message;

	}
?>
