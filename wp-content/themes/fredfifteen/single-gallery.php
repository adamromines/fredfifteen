<?php get_header(); ?>

<div class="bodyContent">

<?php

$images = get_field('images');
$description = get_field('description');

if( $images ): ?>
    <div id="slider" class="flexslider">
        <ul class="slides">
            <?php foreach( $images as $index=>$image ): ?>
                <li>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php
						/*
							Caption display logic
						*/
						$first = False;
						$before = False;
						// Initiate $caption with opening tag, caption class
						$caption = '<div class="caption">';

						// If first image in show, add album description
						if ($index == 0) {
							$first = True;
							$caption .= '<p>' . $description . '</p>';
						}

						if (has_tag('before', $image['ID'])) {
							$before = True;
							$caption .= '<h3 class="before">[Before]</h3>';
						}

						// close div
						$caption .= '</div>';

						if ($first || $before) {
							echo $caption;
						}

						?>

                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div id="carousel" class="flexslider">
        <ul class="slides">
            <?php foreach( $images as $image ): ?>
                <li>
                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

</div>
<?php get_footer(); ?>


