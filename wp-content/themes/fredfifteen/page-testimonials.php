<?php
/*
	Template Name: Testimonial
*/

?>
<?php get_header(); ?>

<!-- Page Content -->
<div class="container">
	<div class="row">
		<!-- Main Column -->
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-body">
			    <!-- Page Heading -->
			    <div class="row">
			        <div class="col-lg-12">
			            <h1 class="page-header">Testimonials</h1>
			        </div>
			    </div>
			    <!-- /.row -->

					<?php

					$args = array(
						'post_type' => 'testimonial'
					);

					$query = new WP_Query( $args );

					?>

				  <!-- Testimonials -->
				  <div class="row">

					<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

			      <div class="col-md-12 testimonial">
				

			          <p class="endorse"><?php

			          the_field('testimonial_text');


			          ?>
			          </p>
			          <h3>
			              <?php the_field('customer'); ?>
			          </h3>
								<hr>
			      </div>


					<?php endwhile; else: ?>
					  <p><?php _e('Sorry, we couldn\'t find any testimonials'); ?></p>
					<?php endif; wp_reset_postdata(); ?>
		
				  </div>
				  <!-- /.row -->
			  </div>
			  <!-- /.panel-body -->
		  </div>
		  <!-- /.panel -->
			
		</div>
	  <!-- /.col-md-8 -->
		
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<?php
					     if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('do_right') ) :
					    endif; ?>
				</div><!--/ panel body -->
			</div><!--/ panel -->
	  </div>
	  <!-- /.col-md-4 -->
		
  </div>
  <!-- /.row -->

</div>
<!-- /.container -->

<?php get_footer(); ?>
